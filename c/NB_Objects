/* Copyright 1998 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/************************************************************************/
/* File:    NB_Objects.C                                                */
/* Purpose: Do routines to do with Toolbox Objects within the Toolbox.  */
/*                                                                      */
/* Author:  Neil Bingham <mailto:neil@binghams.demon.co.uk>             */
/* History: 0.00 - Tue 14th October 1997                                */
/*               - Created.                                             */
/************************************************************************/

/* Library Imports */
#include "Main.h"


/* -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+- MENU CODE +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+- */

/************************************************************************/
/* nb_menu_item_strlen                                                  */
/*                                                                      */
/* Function returns the length of the text in the specified menu entry. */
/* This will include a terminating character                            */
/*                                                                      */
/* Parameters: flags    - flags.                                        */
/*             menu     - the Menu to check.                            */
/*             item     - the menu item to check.                       */
/*                                                                      */
/* Returns:    length of contents (-1 if item doesn't exist).           */
/*                                                                      */
/************************************************************************/
int nb_menu_item_strlen(unsigned int flags, ObjectId menu, ComponentId item)
{
  _kernel_oserror	*er = NULL;
  int			 nbytes = 0;

  NB_UNUSED(flags);

  er = menu_get_entry_text(0, menu, item, NULL, 0, &nbytes);

  if (er != NULL)
  {
    return(-1);
  }
  else
  {
    if (nbytes <= 1)
    {
      /* Menu is empty */
      return(0);
    }
    else
    {
      return(nbytes);
    }
  }
}

/* -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+- PROGINFO CODE +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+- */

/************************************************************************/
/* nb_proginfo_setup                                                    */
/*                                                                      */
/* Function will setup the version number, and all the other info       */
/* included within a proginfo dialogue.                                 */
/*                                                                      */
/* Parameters: flags       - Bit 0 set if licence type to be included.  */
/*             proginfo_id - ObjectId of proginfo dialogue.             */
/*             version_no  - Version number                             */
/*             licence     - Licence Type.                              */
/*                                                                      */
/* Returns:    _kernel_oserror.                                         */
/*                                                                      */
/************************************************************************/
_kernel_oserror *nb_proginfo_setup(unsigned int flags, ObjectId proginfo_id,
                                   char *version_no, int licence)
{
  _kernel_oserror	*er = NULL;
  int			 new_flags = 0;

  /* Program Version field */
  er = proginfo_set_version(0, proginfo_id, version_no);
    if (er != NULL)
    {
      return(er);
    }

  /* Program Licence Options */
  if (nb_bit_set(flags, 0) == TRUE)
  {
    /* Bit 0 is set - include a licence type */
    new_flags = 4;

    er = proginfo_set_licence_type(new_flags, proginfo_id, licence);
    if (er != NULL)
    {
      return(er);
    }
  }
  return(NULL);
}


/************************************************************************/
/* nb_object_is_open                                                    */
/*                                                                      */
/* Function will return TRUE if the passed object is on screen.         */
/*                                                                      */
/* Parameters: flags - Flags word.                                      */
/*             win   - the window to check.                             */
/*                                                                      */
/* Returns:    TRUE or FALSE.                                           */
/*                                                                      */
/************************************************************************/
int nb_object_is_open(unsigned int flags, ObjectId win)
{
  _kernel_oserror	*er = NULL;
  unsigned int		 state = 0;

  NB_UNUSED(flags);

  er = toolbox_get_object_state(0, win, &state);

  if (er == NULL)
  {
    if (nb_bit_set(state, 0) == TRUE)
    {
      return(TRUE);
    }
    else
    {
      return(FALSE);
    }
  }
  else
  {
    return(FALSE);
  }
}
